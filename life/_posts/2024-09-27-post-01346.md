---
layout: post
title: "잘 다니던 회사 퇴사한 이유 1 - 비트코인"
toc: true
---


## 잘 다니던 기업 퇴사한 비대발괄 1 - 비트코인
 때는 바야흐로 2017년. 조선소에서 일용직으로 용돈을 벌 때였습니다.

거기서 아울러 일했던 아저씨들은 쉬는 시간마다 운동경기 토토나 홀짝 같은 도박을 즐겼습니다.
그러다 언제부턴가 업비트로 단타를 치는 무리가 일편 둘 등장하기 시작합니다.
저는 바야흐로 처음으로 '코인'이란 것을 알게 됩니다.

그것이 어디에 쓰이는지, 어째서 코인인지 궁금하긴 했으나 도박이라고 생각되어 관심을 차단하게 됩니다.
그저 새로운 도박인가 보다~ 하고 어쨌든지 지나치게 되죠.
그러나,
불과 몇 중요 지나지 않아서 행동환경 아우 모두가 시퍼런 업비트 창을 켜놓고 부지런히 단타를 치는 진귀한 광경을 보게 되었습니다.

"..?!"

이번엔 무조건 지나칠 수 없었습니다.
생전 보지도 듣지도 못했던 그것을 근간히 하는 사람들을 보며
무언가 이상하게 돌아간다고 느낀 첫 번째 계기였습니다.
며칠간 주변을 관찰하다 보니 호기심은 더욱 커졌고, 그제야 동료들을 따라 업비트를 설치하게 됩니다.
그리곤 소액으로 잡알트를 건들며 용돈벌이를 했던 기억이 납니다.

그렇게 엄청난 변동폭을 즐기며 수익을 맛보았고,
세상에 시고로 게 전부 있구나! 신기한 경험을 하고 있단 생각뿐이었습니다.
그때까지만 해도 저는 재미로만 단타를 치던 애한 사람이었던 거 같습니다.

그런데 채 2주가 지나지 않아 두 번째 이상한 계기를 맞게 됩니다.
때는 11월, 퇴근 최종 석반 하저 시간이었습니다.
식당 TV에선 일모 9시 뉴스가 나오고 있었습니다.
앵커, 기자, 교수가 둘러앉아 비트코인을 놓고 갑론을박 토론을 하고 있었던 겁니다.
그리곤 차트 하나가 나왔는데,, 그 하모 자산도 보여주지 못했던 폭발적인 값 상승률이었습니다.
저는 먹던 공양 숟가락을 내려놓고 뉴스에 빠져들었습니다.

뉴스 자막에는 튤립 버블이니.. 폰지 사기니.. 광기.. 뭐 그런 부정적인 말을 쏟아내고 있었습니다.
그도 그럴만한 것이 평소에 보던 차트 모양이 아니었기 때문입니다..
에베레스트 봉우리 하나가 차트에 높게 솟구쳐 있었으니 말입니다.
 
그러나 저는 직감했습니다.
어쩌면 [바이비트](https://cost-steady.com/life/post-00119.html) 새로운 역사가 시작된 건지도 모른다고.
뉴스에서 하는 말은 들리지 않았고 가슴은 두근거렸습니다.
한 세기에 일어나는 혁명적인 순간을 마주할 복 있는 사람은 과연 몇 명이나 될까요?

당시의 저는 비트코인이 무엇인지 시거에 알진 못했습니다.
IT 지식도 없고, 국민경제학 지식도 없었기 때문입니다.

그저 '사람들이 원하는 무언가가 저기에 있다'는 직관만 존재했습니다.
또,
'새로운 무언가'가 나타나 기존의 인간의 삶을 뒤흔들었다는 건 확실하다고 생각했습니다.
그날은 쓸데없이 기분이 좋았던 거 같습니다.

그런데 웬걸, 2017년 말.
모든 코인이 대폭락을 맞으며 '크립토 윈터'에 접어들었습니다.
비트코인의 상승률을 보며 뜨거워진 가슴이 미처 식기도 전에 다시 물벼락을 때려버린 것입니다.
혼미했습니다.
뭐가 어떻게 돌아가길래 산재 높았던 가격이 한순간에 무너져 내릴 생명 있는 거지?
그 느낌을 비유하자면, 에베레스트에 거대한 눈사태가 나서 순식간에 저를 덮쳐버린 것 같았습니다.
처음 느꼈던 감동이 컸던 탓인지,, 대폭락을 바라보며 느껴지는 허무함과 아쉬움도 컸습니다.
내가 잠시간 꿈을 꾼 것인가?
뭔가 특별할 거라고 생각했는데.. 잠연히 나만의 환상이었나..

그 후로 사변 사람들도 하나 둘 업비트를 지우고 운동경기 토토와 홀짝을 여름철 시작했습니다.
저 아울러 내려가는 차트를 보며 비트코인에 대한 환상을 차츰차츰 접게 되었습니다.

곧이어 조선소를 졸업하고, 여러 활동과 학업을 준비하며
바쁘게 현생을 지내다 보니 비트코인의 존재를 까맣게 잊은 채로 2년이란 세월이 지나게 됩니다.

그렇게 2019년 대담 겨울.
저는 첫 직장을 구해 서울로 올라왔고 가산디지털단지에 자리를 잡습니다.
우연인지 필연인지, 표제 전공과 상관없는 IT 부서에 배정되어 개발자와 나란히 일했습니다.
열심히 일하는데 제호 자아에 균열을 내줄 더군다나 애한 번의 충격적인 뉴스를 맞이합니다.
'코로나'
단 며칠 동 주가가 50% 압미 주저앉았고, 뉴스에서는 누누이 폭락과 비상이란 단어를 반복했습니다.
제 무의식 속에선 2017년에 봤던 에베레스트산 꼭대기가 연상되었습니다.

2020년.
모두가 주식의 흥망성쇠를 주제로 놓고 바쁘게 떠들어댔습니다.
수천만 원을 잃고, 버는 사람들. 울고 웃는 사람들을 보며 과제 활동 처음으로 '돈'에 대해 궁금증이 들었습니다.
돈을 어떻게 버는지가 아니라, 돈이란 무엇인지 이년 자체가 궁금해졌습니다.
도대체 돈이 뭐길래 사람이 일까지 팽개치고 화장실에 박혀 주식 차트를 보게 만들고,
영혼까지 끌어모아 투자를 하게 만드는 건지!
그 많은 돈은 어디서 흘러 어디로 가는지!

그때부터 책과 유튜브 뭐든 상관없이 눈치 궁금증을 해소해 줄 복운 있는 모든 것을 뒤졌습니다.
처음에는 궁금증을 해소하면 해소할수록 더욱더욱 혼란스러웠습니다.
마치 바닷물을 마시는 것처럼요.
그렇게 파도 파도 끝이 없는 심연을 헤매이다 '익숙한 무언가'를 만나게 됩니다.
화려한 폭락을 결국 3년간 안부를 알 수 없었던 비트코인이 거기에 있었습니다.

비트코인을 만나기 전,
국내 주식, 외방 주식, ETF, 채권, 부동산과 함께 일반인이라면 고사 가질
모든 자산을 얕지만 넓게 경험해 본 가 하나를 알게 되었습니다.
'자산의 가격은 막막히 올라갈 수밖에 없는 시스템 속에 있다.'

처음에는 이게 가능한 것인지 상식적으로 혼란스러웠습니다.
결론은 가능했고, 앞으로도 그럴 것입니다.

그리고 생각했습니다.
세상에 이렇게 돈이 많은데, 또 다시 만들어질 돈이 이렇게 많은데..
가만히 놔둔다면 한량없이 늘어날 익금 돈이 도무지 뭐길래 나의 유한한 써서 오로지 최저시급을 받고 있는지.

그때부터 뭔가 잘못됐음을 깨달았고 올바른 방법을 찾기 시작했습니다.
닥치는 목표 자료와 책을 찾았습니다.
또 배운 내용을 언저리 사람과 공유하는 시간도 가졌습니다.
직장 동료, 둘레 친구, 아녀자 친구, 가족..
그러나 반응은 냉담했습니다.
대충 세어봐도 100명은 되는 인계 복판 이식 문제에 대해 진지하게 고민해 본 사람은 단 극한 명도 없었습니다.

대부분의 사람들은 도로 나오는 게임이 무엇인지, 새로운 핫플레이스 어디가 좋은지, 주말에 어디서 술 마셨는지, 이번에 나온 브랜드... 등
암담했습니다.
그나마 희망이라고 느꼈던 주식, 부동산, 재테크에 관심이 있는 친구들 조차 가액 현 자체의 성질은 너무너무 관심이 없었습니다.
그저 새로운 장사 아이템을 구상하거나, 부동산 발품을 팔거나.. 수익률 좋은 투자 사음자리표 찾기에 집중할 뿐이었습니다.

'돈을 극히 버는 방법' vs '돈을 이해하는 것'
돈을 이해하는 것이 돈을 듬뿍 버는 방법이라고 생각한 저는,
그때 처음으로 다짐하게 됐습니다.
내가 나서야겠다고.

...

그때가 2022년 봄이었으니 이자 글을 쓰는 지금은 앞서 1년이 보다 지났습니다.
저는 이득 일이 진정으로 중요하다고 생각되어 퇴사를 했습니다.
오직 글쓰기에 전념하겠다는 목표를 세우고서요.
블로그를 하는 이유는 인터넷에 퍼진 원료 중에 잘못된 정보가 너무나도 많기 때문이다.
이것들을 고대 잡지 않으면 많은 사람들이 혼란을 느끼게 되고, 억울하게 기회를 놓치게 될 거라 생각했습니다.

대중에 노출된 비트코인 관계 재료 대부분은 악플러나 트레이더에 의해 왜곡되거나,
기술자들이 써서 그런지 일반인 눈높이에 맞지 않는 어렵고 재미없는 글이 많습니다.
2017년 당시만 해도 제가 비트코인을 이해하는데 어려움이 많았지만,
2020년 개발자로 취업하게 되면서 IT 지식을 쌓게 되면서 비트코인을 이해하는데 큰 도움이 되었단 걸 느낍니다.
이 경험을 살려서 글을 쓰면 많은 분들에게 도움이 될 거라 생각합니다.

또 정상 가난히 비트코인을 평가절하하는 글, 코인으로 손해를 본 결과 감정에 뇌가 지배되어 쓴 악플 글, 시황도 모른 미처 감으로 때려 쓴 글, 코인 가격'만' 중요시하는 글, 인사이트를 알기 어려운 단일 정보 위주의 글..
이런 것들을 익금 블로그에서 교정한 성실 부드럽게, 소화하기 편하게 전달하고자 합니다.

저는 비트코인 이념과 이이 업적을 존경하는 어떤 사람으로서,
남들보다 비트코인을 다소간 더욱더 사고 있게 지켜봐 온 사람으로서
비트코인의 우수함을 대중에게 전달할 의무가 있다고 생각합니다.

블로그의 목적은 명확합니다.
비트코인을 오해해서 손실 보는 사람이 없도록.
그리고 21세기 최고의 발명품으로 인정받게 될 비트코인의 가치를 여러분이 하루라도 쏜살같이 깨달을 운명 있도록.
글을 써보려 합니다. 감사합니다

### 태그

### 댓글0
