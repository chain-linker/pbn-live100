---
layout: post
title: "[펌]바츠해방전쟁 in 리니지2"
toc: true
---



혁명이 태양처럼 빛나던 날
이토록 많은 혈맹이 집결했지만 바츠동맹군은 아직도 수적으로 DK연합군에 비해 열세였다. 이러한 역학관계는 바츠 서버의 독특한 정치적 정세에서 비롯된다. 바츠 해방전쟁이 일어나기 전까지 현실적으로 DK연합에 가입하거나 양해를 얻지 않고서는 자신의 캐릭터를 52레벨 희망 육성하는 것이 많이 어려웠다. 52 이상의 손수평기 업을 위해 천하없어도 들어가야 하는 사냥터를 DK연합이 독점하고 있었기 때문이다. 개인으로 파편화된 사용자들은 물적 안락과 사회정의 사이에서 자작 현실과의 타협을 선택했다. 임자 귀결 전쟁을 할 수 있는 대부분의 고레벨 사용자들은 이익 시기까지도 DK연합에 속해 있었다.
이렇듯 강력한 전투력을 가진 DK연합군 전사들은 DK혈맹의 총군주이자 지배4혈의 총군인 ‘shadow여솔’의 지휘 기초 일사불란하게 움직이고 있었다. ‘shadow여솔’ 밑에는 혈맹전쟁 참전 경험이 풍부한 백전노장들이 포진하고 있었다. 신의 기사단혈맹의 총군주 ‘지존군주’, 위너스혈맹의 총군주 ‘푸른 전사’, 정혈맹의 총군주 ‘만월의 폭군’이 그들이다.
결론부터 사실상 바츠동맹군이 대승을 거둔 아덴 공성전은 기만전술의 승리였다. 그토록 많은 내복단이 참전했음에도 바츠동맹군은 전투가 시작해 끝날 때까지 한 번도 실지로 전력에서 DK연합군보다 우위에 있지 못했다. 불리하지만 회피할 행운 없는 자전 전투에서 바츠동맹군 수뇌부는 기만전술을 선택했다.
기만전술이란 위장과 은폐의 생각 의도를 가진 군사행동이다. 전쟁에서 일정 신수 적을 속이기 위해 대병력을 양동작전에 투입하는 것보다 한층 위험한 작전은 없다. 일찍 클라우제비츠는 “기만전술이 계획된 소기의 목적을 달성한 경우는 주로 없다”고 지적하면서 “지휘관은 책략을 동원하기보다 쌍방 전투력의 냉엄한 현실을 직시하면서 오직 필연성만을 고려하는 ‘엄숙한 열의’를 가져야 한다”고 말한 바 있다.
수적 열세를 기만전술로 극복

이런 관점에서 바츠동맹군의 승리는 기적이었다. 수많은 내복단 복판 첩자가 있어서 채팅창의 귓속말에 다다 제한 줄만 입력했다면 발각될 운 있었을 기만전술이 두 번이나 성공했다. 피차 얼굴조차 본 대조적 없는 사이버 공간에서 내복단 동지들은 진실 공간에서보다 썩 철저한 도덕성을 보여주었다.
바츠동맹군의 기만전술은 공성 등록부터 시작됐다. ‘리니지2’의 경합 규칙에 따르면 양군은 공성 머릿결 24시간 전에 공격할 성으로 내찰 수성 등록과 공성전 등록을 해야 한다. 등록 끝판 10분 전. 제네시스혈맹을 제외한 바츠동맹의 모든 혈맹은 오랜성에 수성 등록을 했으며 제네시스혈맹만이 아덴성에 공성 등록 절차를 밟고 있었다. 바츠동맹군은 누가 봐도 DK연합군의 탈환전에 대비해 오랜성 방어에 전념한 것처럼 보였다.
등록 극단 8분 전. 제네시스혈맹마저 공성 등록을 취소하고 어딘가로 사라졌다. 이에 DK연합군은 아덴성 수성 등록을 취소하고 오랜성으로 이동하는 한가지 바츠동맹군의 위치를 맹렬하게 찾았다. 플러스 때 사라진 제네시스혈맹과 바츠동맹군 본대는 사냥꾼 향보 근처에 매복하고 있다가 DK연합군의 이동 정보를 받자 즉시 아덴성 마을로 달려갔다.
등록 뒤 3분 전 바츠동맹군의 위치를 파악하지 못해 우왕좌왕하던 DK연합군은 할 호운 없이 오랜성에 공성 신청을 했다. 같은 동안 바츠동맹군은 아덴 공성에 26개 혈맹이 신청하는 데 성공한다. 지우금일 아덴성에 수성 등록한 것은 DK의 1개 라인혈맹에 불과했다. 양동작전의 기만전술로 바츠동맹군은 공성에 참여할 호운 있는 병력면에서 우위를 점하게 된 것이다.
그러나 7월17일 오후 상당히 공성이 쉬울 것이라던 바츠동맹군의 예상은 빗나갔다. DK연합군은 오후 7시부터 아덴성 주위에 무궁무진히 밀려들었다. 그들은 엄청난 숫자로 대오를 정비하고 전략적 요충지마다 바츠동맹군의 진격을 봉쇄하기 위한 요격진지를 구축했다.
숙련된 DK연합군은 품성 입구 중간에 칼과 단검, 창을 든 격수 부대를 배치하고 처녀 옆으로 넓게 궁수 부대를 포진시킨 학익진(鶴翼陣)을 구축했다. 이것은 성문으로 돌진하는 바츠동맹군을 일점사(一點射)로 저지하기에 양반 효율적인 진법이었다.
8시. 결전이 시작되자 최전선에 DK연합군의 맹장 아키러스가 이끄는 ‘전 서버 최강의 전투 부대’ 아키러스 파티(9명)가 나타났다. 아키러스 파티는 안자 깜짝할 사이에 바츠동맹군 3개 파티를 전멸시키고 바츠동맹군의 최전선 진지를 파괴했다. 전력의 우열이 형편없이 명백하게 드러났다. 애오라지 수모 희생을 치르더라도 물러서지 않겠다는 의지만이 바츠동맹군을 버티게 하고 있었다.
9시. 무수한 희생에도 바츠동맹군은 진지조차 세우지 못했다. 공성군측이 1시간이 지나도 진지를 세우지 못했다는 것은 치명적인 전황이다. 공성군측이 전사했을 뜰 진지가 있으면 인제 진지에서 부활할 행운 있지만 진지가 없으면 두 번째로 먼 마을에서 부활해 10여 분을 달려와야 주하 때문이다. 이런즉 상황에서 바츠동맹군의 두 번째 기만전술이 시작됐다.
인간 바리케이드로 연합군 회군 저지

9시10분. 바츠동맹군은 부서진 진지를 뒤로하고 산지사방으로 패주하기 시작했다. DK연합군의 눈에 이와 같은 패주는 자연스럽게 보였다. 상대는 총사령관조차 정해지지 않은, 호상간 얼굴도 대뜸 모르는 혈맹들의 엉성한 결합체였고 내복만 달랑 걸친 오합지졸의 군대였다. DK연합군의 맹공에 1시간 길이 버틴 것이 이상할 정도였다.
9시20분. 승기를 잡은 DK연합군은 진군했다. 패주했지만 적의 주력이 깨끗이 분쇄되지 않았기 때문이다. 연합군 수뇌부는 결정적인 승리를 획득하기 위해 아덴성 주변의 전장을 떠나 오랜성으로 추격전을 결정했던 것이다. 뿐만 아니라 만약에 대비해 CC지역의 레드군단 궁수부대와 DD지역의 화이트군단 궁수부대를 잔류시켰다.
그러나 입때껏 바츠동맹군은 패주한 것이 아니었다. 패주하는 것처럼 보이는 기만전술을 폈던 것이다. 바츠동맹군은 대다수 흩어지지 않고 전장 외곽에 집결해 매복했다.
오랜성으로 진군한 DK연합의 대군은 리벤지혈맹을 비롯한 소부대만이 지키고 있는 오랜성을 맹공했다. 외성문 바깥쪽에 공성 진지를 구축하고 공성골렘(성문을 부수기 위한 공성무기)을 뽑아 주의 깜짝할 사이에 외성문을 부수어버렸다. 이자 공세는 외성문 안쪽에 압살롬 진형(원형 일점사 진형)을 구축하고 있던 리벤지 혈맹으로 파도처럼 밀려들었다.
리벤지혈맹은 총군주 ‘나리타’와 라인군주 ‘야적’ ‘어시장’ 등 지휘부가 직접 나서서 뚫린 외성문 안쪽에서 절망적인 심정으로 방어했다. 그렇기는 해도 시간이 지나자 DK연합군의 공격이 둔화되기 시작했다. 아덴성의 급전이 오랜성 공성부대로 날아든 것이다.
시간을 되돌려보면, 아덴성의 주전장에서는 DK연합군의 주전력이 이동하자 바츠동맹군이 시거에 새로이 기동했다. 기위 칼리츠버그의 진두지휘 하 아수라처럼 분전한 제네시스혈맹이 DD지역의 화이트 군단 궁수부대를 격파했다. 제네시스혈맹은 한 라인을 보내 @지역에 진지를 구축하는 한쪽 잔 병력으로 전장을 가로질러 CC지역 레드군단 궁수부대의 배후를 엄습했다. 레드군단 궁수부대는 앞뒤로 포위돼 전멸했다.
곧이어 바츠동맹군은 공성골렘을 소환했고 프로핏의 버프를 일체 받은 공성골렘은 일껏 몇 분 만에 외성문을 파괴하고 내성문마저 부수어버렸다. 아덴성으로 쇄도한 바츠동맹군은 망루와 성벽을 지키던 위저드(공격수 마법사) 부대를 격파하고 내성으로 뛰어들었다. 내성을 지키던 DK 골드라인 혈맹은 수적 열세를 극복하지 못하고 시골 사살됐다. 이빨 전투에서 지배4혈의 총군 shadow여솔도 전사했다.
이런 충격적인 소식을 접한 DK연합군은 다급한 기타 오랜성에서 아덴성 마을로 텔레포트해 전장으로 직행하려 했다. 그렇지만 이들을 맞이한 것은 내복단의 결사적인 저항이었다. 평생 바리케이드를 형성한 내복단은 화살받이가 돼 죽으면서 자신들의 시체로 나들이 입구를 겹겹이 막았다. 시체 그렇게 걷기조차 어려워진 DK연합군은 바츠동맹군 궁수 부대와 위저드 부대의 표준화 포화를 받고 쓰러져갔다. DK연합군이 전장에 진입하지 못하는 거리 제네시스혈맹의 칼리츠버그 총군주가 각인실에서 심금 점령을 각인하는 데 성공했다.
이날 PC방에서는 눈물을 흘리며 흐느끼는 사용자들이 목격됐고 경합 안에서는 아덴성의 메인 홀에서 내복단들이 춤을 추었다. 이날은 ‘바츠 해방의 날’로 선언됐다. 과실 날의 혁명은 모든 리니지 월드에 태양처럼 빛났다.
아덴 공성전은 바츠 해방전쟁의 분수령이었다. ‘리니지2’ 월드의 정치적 중심지인 아덴성을 점령한 것을 기점으로 바츠동맹군은 빠른 속도로 분열하며 타락해갔다.
분열의 씨앗은 전승(戰勝)의 과실을 누가 조카 것인가였다. 예컨대 아덴 공성전의 성공으로 리벤지혈맹은 오랜성을 차지했고, 제네시스혈맹은 아덴성을 소유하게 됐다. 바꿔 말하면 처음부터 바츠동맹군의 선봉을 맡아 많은 희생을 치른 붉은혁명혈맹은 얻은 것이 없었다. 아덴성 각인을 함으로써 아덴성을 소유하게 된 제네시스혈맹은 바츠동맹군이라고는 그렇지만 불과시 3주 전까지 지배연합군의 일원이었다.
제네시스혈맹은 제네시스혈맹대로 가군 병력이 많은 만치 고생은 자기네가 거개 했는데 이전에 지배연합군에 속해 있었다는 묘한 물계 그렇게 너무너무 많은 것을 양보했다고 억울해 했다. 이렇게 논리적으로는 납득해도 심정적으로는 전혀 받아들일 복 없는 데 따른 불만이 하지 혈맹마다 쌓이기 시작했다.
혁명군의 분열과 역전

DK연합군이 아덴성에 방금 기란성마저 빼앗기고 오만의 탑 9층으로 퇴각하자 승리의 전리품을 둘러싼 다리갱이 혈맹간 갈등은 일층 심각해졌다.
이 물 바츠동맹군 소속의 혈맹들이 약칭 ‘용던’이라는 안타라스의 동굴에서 부분적인 통제와 세월 행위를 한다는 비난이 청우 시작했다. 다리깽이 혈맹의 총군주들은 용던이라는 사냥터에 독점구역을 확보함으로써 심리 소유를 둘러싼 혈맹원들의 불만을 무마하려 했다. 오히려 이것은 바츠동맹군의 생존 기반을 뒤흔드는 치명적인 사건이었다.
애초에 바츠동맹군이 외친 ‘정의와 자유’의 구호는 무척 지시적인 의미를 가지고 있다. 이때의 정의는 전반 사용자를 죽이고 연광 프로그램을 돌리며 게임의 룰을 일탈한 지배혈맹에 대한 정의였다. 또한 이때의 자유는 모 사냥터든지 나란히 게임을 하는 사람이라면 누구나 갈 삶 있고 누구나 사냥할 생령 있다는 의미의 자유였다.
그런 대의명분을 내세운 바츠동맹군이 지배혈맹과 똑같은 통제와 오토, 척살을 행했다. 그것은 그들을 지지해온 전반 사용자들의 신뢰를 뿌리째 배신하는 것이었다. 바츠동맹군의 사변 혈맹들은 자신의 혐의를 부정하고 상대방을 비난하면서 아직껏 완전히 섬멸되지 않은 파트너 앞에서 자중지란에 빠지고 말았다. 수세에 몰려 있던 DK연합군은 지금껏 거듭 패치(patch)된 ‘오만의 탑’에 숨어 은인자중 힘을 기르고 있었다.
적의 무서운 잠재력을 외면한 바츠동맹군은 승리에 도취해 사분오열했다. 붉은혁명혈맹은 어제까지 동지였던 리벤지혈맹과 전쟁에 돌입했으며 즉 제네시스혈맹과도 전면전에 들어갔다. 정형 잔여 수적으로 열세에 몰린 붉은혁명혈맹은 과거의 주적이던 DK연합군과 제휴함으로써 바츠 해방에 참전한 사람들을 아연하게 만들었다.
“4혈도 나쁘지만 반4혈도 나쁘다”는 공감대가 바츠 서버에 유포되면서 아덴 공성전까지 일사불란하게 유지된 단합은 무너졌다. 내복단 역 내복단을 빙자한 강도들, 쉬 ‘제조’들이 등장하면서 도덕성을 믿을 수명 없는 경계와 의혹의 대상이 됐다.
끝나지 않은 바츠 해방전쟁

바츠동맹군의 타락과 분열로 전세는 역전됐다. DK연합군은 차츰차츰 조금씩 빼앗긴 성들을 모조리 탈환했으며 혁명군의 공성전을 성공적으로 방어했다. 그리하여 해가 바뀐 2005년 1월27일 DK혈맹은 다시금 무제한 척살령을 발동했고 ‘리니지2’의 전반 [리니지 프리서버](https://imgresizings.com/life/post-00036.html) 사용자들은 바츠 해방의 꿈이 비참하게 좌절됐음을 확인해야 했다.
2005년 6월 금시 바츠 서버는 해방전쟁 이전의 참상으로 되돌아왔다. 사냥터에서 거치적거린다는 이유로 날 저녁에 700명이 넘는 사용자가 지배혈맹에 의해 살해되고, 산발적인 소요가 일어나고, 형씨 여분 DK연합군이 사냥터의 희월 행위를 통해 만들어내는 바츠 서버의 아덴 가격은 폭등한다.
그러나 바츠 해방전쟁 스토리는 여태껏 도시 끝난 것이 아니다. 사용자들은, 미약하지만 아직도 자신이 하는 게임이 바츠 해방전쟁 절정기의 네년 숭고한 감정을 실어 나르는 매체, 숭고의 감정을 불러일으키는 살아 있는 물건으로 변모하는 현상을 목격한다.
숭고란 뭔가 고귀하고 성스럽고 영웅적인 것이 자신의 눈앞에 현전(現前)하고 있다는 충격의 체험이다. 그것은 묘사할 중심 없고 설명할 중앙 없는 천지창조의 순간을 연상시킨다. 그런 의미에서 아직도 ‘리니지2’의 스토리는 날마다 놀랍고 비일상적이며 충격적인 순간, 표백 불가능한 것이 일어나는 순간의 미학, 숭고의 미학에 의해 지배된다.
2005년 5월의 어느 날, 사용자들은 아직도 저항하고 있는 극소수 혈맹 서울 범위 파티가 공상 계곡에서 안타라스의 동굴로 출정하는 것을 본다. 시간은 벌써 9시가 넘은 아침이다. 너 파티의 주인공들은 총체 밤을 새웠다. 수백명의 DK혈맹원과 벌인 간밤의 싸움에서 많은 혈맹원의 캐릭터가 더는 활동할 생명 없는 봉인 상태에 이르렀다. 살아남은 우인 기축 두 사람은 D급 무기를 들고 있었다. 무수한 죽음으로 레벨이 30 마지막 다운돼 무의미할 정도로 공격력이 낮은 무기를 들고, 옥쇄할 수밖에 없는 전쟁터로 묵묵히 떠나가는 것이다.
함께 파티 사냥을 하며 성장한 친구들은 대개 현실과 타협했다. 친구들은 저마다 이래도 좋고 저래도 좋은 ‘친목혈’을 꾸려 군주가 되고 대전 안에서 편안한 인생을 살아간다. 그럼에도 불구하고 지배권력에 대한 저항의 길을 택한 혈맹원들은 사냥터도 없이 풍찬노숙하며 사방에서 공격받고 악명을 뒤집어쓴다. 외로운 성적 따뜻한 내용 한마디에 쉽게 정을 주었다가 사기를 당하기도 한다.
이런 외로운 전사들이 묵묵히 전쟁터로 나가고 있는 것이다. 이런즉 광경은 ‘리니지2’의 사용자만이 이해하고 감지할 수 있는 ‘숭고’다. 이러한 전순간 ‘리니지2’의 스토리는 위엄을 갖춘 희생자들, 최후에 승리하는 패배자들, 타락한 현실에 대해 선(善)을 주장하는 무법자들의 형이상학적이고 영웅적인 진실을 전달한다.
숭고한 체험, 귀환하지 않은 영웅들
일찍이 조셉 켑벨은 많은 스토리에서 잊을 수 없는 체험을 경계 영웅이 평범한 일생 세상으로 귀환하는 데 어려움이 따른다고 지적한 바 있다. 영웅은 평범한 세계에서 ‘낯설고 특별한 세계’로 들어가 통과제의의 성격을 갖는 고통스런 체험을 한다. 아울러 남들이 경험하지 못한 너 세계로부터 모 물질적, 정서적 전리품을 들고 되처 평범한 세계로 돌아와 사람들을 널리 이롭게 해준다.
그러나 이빨 같은 ‘분리-통과제의-귀환(seperation-initiation-return)’의 구도가 노 지켜지는 것은 아니다. 모든 것을 채우고 모든 것을 견디는 사랑과 신비하고 정복되지 않는 힘과 불멸하는 우주의 그림자를 맛본 영웅은 삶을 매우 몹시 보고 무지무지 깊이 본다. 마침내 그는 안일무사한 생활인의 세계, 평범한 인간의 세계로 돌아올 요체 없는 것이다.

바츠 해방전쟁의 스토리를 체험한 상당수 ‘리니지2’ 사용자야말로 귀환하지 않는 영웅이다. 군 전쟁은 사실상 시간으로는 12개월에 불과하지만 30분이 하루인 ‘리니지2’의 가상현실에서는 무려 48년 짬 계속됐다. 서버를 초월해 모든 ‘리니지2’ 사용자가 숨죽이고 전쟁의 추이를 관찰했으며 자네 고귀한 희생은 많은 반대 심금을 울렸고 당신 허무한 결말은 사용자들 사이에 절망과 냉소주의를 유포했다.
온라인 오락 스토리만이 줄 길운 있는 실리 같은 서사적 감동과 사상적 깊이를 체험한 사람들은 두번 새삼 예전과 같은 사람일 수가 없다. 그들은 ‘폐인’이라는 조롱을 웃어넘기며 온라인 게임이 만든 매트릭스로 날마다 들어간다. 이익금 귀환하지 않는 영웅들이 어떻게 현실로 돌아와 세계를 복되게 할 삶 있을 것인가 하는 문제는 가상현실과 현실의 융합이 중요한 관심사로 떠오른 디지털 매개체 시대에 찬찬히 고려해야 할 지점이다.   (끝)

 /이인화 8.21.도깨비뉴스
