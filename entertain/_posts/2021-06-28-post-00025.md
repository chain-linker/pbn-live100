---
layout: post
title: "넷플릭스 드라마 추천!! 베르사유, 끊을 수가 없어요."
toc: true
---

 안녕하세요.
 주말에는 아무래도 귀루 여유가 있으니 누구 것을 해도 좋아요.
 전 대부분 책을 본다거나 영화를 보거나 하는데요.
 어제오늘 넷플릭스에서 좋은 영화와 드라마가 있어서 보고 있어요.
 어제(토요일)는 베스사유 시즌 1을 경우 시작했어요.
 넷플릭스는 시리즈의 중간에 곧장 둘 수가 없어서 졸려서 눈을 비비는 지경이 [무료영화](https://bellpretend.gq/entertain/post-00002.html) 될 때까지 보게 되죠. ㅠㅠ
 

 2016년 10월에 프랑스 파리를 여행한 대조적 있어요.
 자유롭게 했던 여행이라 파리시내에서 기차를 타고 베르사유까지 갔고, 하루를 베르사유 궁전에서 놀았어요.
 루이 14세가 만들어놓았다는 거대한 궁전의 면면촌촌 저곳을 봤죠.
 

 

 영리 사진은 입구쪽인데, <베르사유>에도 각색 등장하는 곳입니다.
 지금은 가고 싶어도 쉽게 갈 명맥 없다는 것이 비현실적이네요.
 짜장 넓고 화려한 곳이었고, 이것을 만들 생목숨 있는 힘이 어느정도인지 가늠이 되지 않더라구요.
 

 

 드라마에 나오는 것처럼 처음에는 전 왕의 사냥터의 작은 건물이었던 것이 루이 14세가 증축해서 거대하고 화려한 성이 되었고, 지금은 국제 각국의 관광객들이 십상 가보는 관광지가 되었죠.
 드라마에 나오는 정원사와의 대화가 굉장히 재미있었어요.
 뭔가 한복판 풀리는 게 있으면 퇴역군인출신인 정원사에게 슬슬 물어보죠.
 그럼 으레 선문답처럼 정원사가 답을 꼭쇠 가르쳐줘요.
 무엇 왕의 마음을 서기 아는 것처럼 말이죠.
 

 

 베르사유궁전의 정원과 운하는 정녕히 아름답죠.
 시간이 난다면 수지 넓은 상황 곳곳을 돌아보고, 분수쇼도 보고 음악도 들으면 좋아요.
 

 넷플릭스의 희곡 <베르사유>는 여러가지 포인트를 알아두면 일층 재미있게 볼 수명 있을 거 같아요.
 루이14세가 디자인한 베르사유 궁전에 대한 이런저런 이야기와 루이14세 즈음에 벌어졌던 프랑스의 정치와 유럽의 정치세계 더구나 루이14세의 여자들을 알면 좋을 거 같아요.
 

 오늘은 루이 14세에 대해 게다가 이녁 물 프랑스의 정치적인 여러 사건들에 대해 정리를 할 겁니다.
 

 우선, 루이14세는 누구일까요?

1638년에 태어나서 1715년에 죽은, 우리에게는 '짐이 바로 국가다'라는 프랑스 절대왕정의 상징으로 기억되는 인물입니다.
특히 더욱 깊이 각인되어 있는 이유 중 하나가 바로 지금 남아있는 베르사유궁전의 화려함때문일 건데요.
넷플릭스 드라만 <베르사유>의 주인공인 루이 14세는 아무 사람인지 알고 드라마를 보는 것은 드라마의 재미를 더욱 배가시키는 일입니다.
루이14세는 5살이 되기 전에 왕에 올랐어요.
드라마에는 언제나 표현되어 있지 않지만, 이탈리아 추기경 쥘 마자랭이 뒤에서 통치를 도왔다고 합니다. 성인이 될 때가지요.
루이14세는 아버지가 만들기 시작한 중핵 집권화를 유지 추진해서 프랑스의 지방에 남아있던 봉건제도의 잔재를 청소하고, 자신이 내리는 지시를 듣도록 했어요.
그러나 지방의 힘있는 귀족들은 반란을 일으켰죠. 루이14세는 베르사유 궁전을 짓고 이들은 자신의 곁으로 불러 올려
사치스러운 생활을 즐기게 하면서 약화시켰고, 결국 절대 군주로 자리매김하게 됩니다.
 드라마에서 보면 귀족임을 증명하는 증명서를 요구하죠.

 뿐만 아니라 이들을 베르사유로 불러올려 자신에게 무릎을 꿇는 퍼포먼스라도 하는 걸 즐기죠.
루이14세는 키가 작았다고 해요. 그래서 키에 대한 콤플렉스로 하이힐을 최초로 신었고, 우습게도 귀족들이 따라하면서 유행이 되었다고 합니다.

루이14세 당시의 이야기는 사실 그의 아버지였던 루이 13세 때의 정치적, 사회적 사건들을 이해하면 더 알기 쉬운데요.
루이 14세가 어렸을 때 프랑스에는 두 개의 커다란 내전이 일어나죠.
하나는 종교전쟁이구요. 무려 1618년에서 1648년까지 30년 동안이나 일어나서 30년전쟁으로 더 알려졌어요.
루이14세는 이 전쟁이 끝날무렵 겨우 10살이었는데, 이때는 프롱드의 난이라는 귀족들의 반란이 또 일어나죠.
이 드라마에서 나오는 개신교들의 반란은 곧장 30년전쟁에서 이어진 거라고 볼 명 있어요.

루이 14세는 절대왕정을 구축하는 일에 목을 메죠.
가장 화려한 궁정을 지어 힘을 보여주고, 종교를 가톨릭으로 통일하는 것이 유리할 것으로 생각해, 선대가 발표했던 낭트칙령을 폐지하죠. 이 낭트칙령은 프랑스 내 개신교 신자들을 공직자 취임제한 등의 차별로부터 보호하는 차별금지정책이었다고 하는데, 이 것이 폐지되면서 위그노가 탄압을 받게 됩니다.
이들은 종교의 자유가 인정되는 이웃나라 - 영국이나 네덜란드-로 도망을 상호 됩니다.
또한 루이 14세는 많은 전쟁을 일으켜요.
루이 14세는 재위기간이 72년이 넘는데요. 그 중 31년동안 전쟁을 하게 됩니다.
징병제도도 만들어 프랑스군이 근대화가 이루어지게 되죠. 반면에 드라마에 나오듯이 군인들의 불만이 극에 달하기도 합니다. 날찍 드라마에서도 궁전을 짓는 일을 하던 퇴역군인들이 파업을 하면서 왕에게 자신들을 대뜸 대우해줄 것을 요구하고 있어요. 그러므로 왕이 앵발리드를 짓게 합니다.
파리에 가면 앵발리드라는 금색의 돔으로 된 건물이 있는데, 이게 바로 노병들에게 생활할 수 있게 만든 곳입니다.

드라마 <베르사유>를 더 재미있게 보기 위해서는 이런 내용말고도 알아두면 좋을 게 너무 많아요.
하나 하나 알아가면서 볼 예정입니다.
 

 다음에는 루이 14세의 여인들에 대해서 알아볼까 해요.
 

 아래의 사진은 2014년 베르사유에서 찍었던 사진들입니다.
 최하층 추억을 소환하는 의미에서.....

